package model

type Message struct {
	ID         uint64 `gorm:"primaryKey" json:"id,omitempty"`
	SenderID   uint64 `gorm:"column:sender_id" json:"userid,omitempty"`
	TargetID   uint64 `gorm:"column:target_id" json:"dstid,omitempty"`
	Msg        string `gorm:"column:message" json:"content,omitempty"`
	CreateTime int    `gorm:"autoCreateTime; column:ctime" json:"ctime"`
	UpdateTime int    `gorm:"autoUpdateTime; column:mtime" json:"mtime"`
}

func (m Message) TableName() string {
	return "message_tab"
}
