package model

type Session struct {
	ID         uint64 `gorm:"primaryKey" json:"id" form:"id"`
	UserID     uint64 `gorm:"column:user_id" json:"ownerid" form:"ownerid"`
	FriendID   uint64 `gorm:"column:friend_id" json:"dstobj" form:"dstobj"`
	Status     int    `gorm:"column:status" json:"status"`
	CreateTime int    `gorm:"autoCreateTime; column:ctime" json:"ctime"`
	UpdateTime int    `gorm:"autoUpdateTime; column:mtime" json:"mtime"`
}

func (r Session) TableName() string {
	return "session_tab"
}
