package model

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var _db *gorm.DB

func init() {
	dsn := "root:Qy123456@tcp(127.0.0.1:3306)/core_db?charset=utf8mb4&parseTime=True&loc=Local"
	var err error
	_db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		panic(" error=" + err.Error())
	}

	sqlDB, _ := _db.DB()

	sqlDB.SetMaxOpenConns(100)
	sqlDB.SetMaxIdleConns(20)

	//_db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(&User{})
}

func GetDB() *gorm.DB {
	return _db
}
