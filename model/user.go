package model

type User struct {
	ID         uint64 `gorm:"primaryKey" json:"id" form:"id"`
	UserName   string `gorm:"uniqueIndex:uniq_user_name" json:"user_name" form:"user_name"`
	PassWord   string `gorm:"" json:"pass_word" form:"pass_word"`
	NickName   string `gorm:"" json:"nickname" form:"nickname"`
	Status     int    `gorm:"" json:"status" form:"status"`
	Token      string `gorm:"" json:"token" form:"token"`
	CreateTime int    `gorm:"autoCreateTime; column:ctime" json:"ctime" form:"ctime"`
	UpdateTime int    `gorm:"autoUpdateTime; column:mtime" json:"mtime" form:"mtime"`
}

func (u User) TableName() string {
	return "user_tab"
}
