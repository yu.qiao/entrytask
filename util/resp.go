package util

import (
	"encoding/json"
	"log"
	"net/http"
)

type ResponseData struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data,omitempty"`
}

type H struct {
	Code  int         `json:"code"`
	Msg   string      `json:"msg"`
	Data  interface{} `json:"data,omitempty"`
	Rows  interface{} `json:"rows,omitempty"`
	Total interface{} `json:"total,omitempty"`
}

func RespFail(writer http.ResponseWriter, msg string) {
	Resp(writer, -1, nil, msg)
}

func RespOk(writer http.ResponseWriter, data interface{}, msg string) {
	Resp(writer, 0, data, msg)
}

func RespOkList(w http.ResponseWriter, lists interface{}, total interface{}) {
	RespList(w, 0, lists, total)
}

func RespList(w http.ResponseWriter, code int, data interface{}, total interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	h := H{
		Code:  code,
		Rows:  data,
		Total: total,
	}

	ret, err := json.Marshal(h)
	if err != nil {
		log.Println(err.Error())
	}

	_, _ = w.Write(ret)
}

func Resp(writer http.ResponseWriter, code int, data interface{}, msg string) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	rep := ResponseData{
		Code: code,
		Msg:  msg,
		Data: data,
	}

	ret, err := json.Marshal(rep)
	if err != nil {
		log.Panicln(err.Error())
	}

	_, _ = writer.Write(ret)
}
