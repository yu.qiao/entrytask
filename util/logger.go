package util

import (
	log "github.com/sirupsen/logrus"
	"os"
	"path"
)

var (
	debugFile, infoFile, warnFile, errorFile *os.File
)

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	logDirPath := ""
	if dir, err := os.Getwd(); err == nil {
		logDirPath = dir + "/logs"
	}

	//var err error
	var fileName string
	fileName = path.Join(logDirPath, "debug.log")
	debugFile, _ = os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)

	fileName = path.Join(logDirPath, "info.log")
	infoFile, _ = os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)

	fileName = path.Join(logDirPath, "warn.log")
	warnFile, _ = os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)

	fileName = path.Join(logDirPath, "error.log")
	errorFile, _ = os.OpenFile(fileName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	/*if err != nil {
		fmt.Println("err", err)
	}*/

}

func LogDebug(msg string) {
	log.SetOutput(debugFile)
	log.WithFields(log.Fields{
		//"animal": "walrus",
		//"size":   10,
	}).Debug(msg)
}

func LogInfo(msg string) {
	log.SetOutput(infoFile)
	log.WithFields(log.Fields{
		//"animal": "walrus",
		//"size":   10,
	}).Info(msg)
}

func LogWarn(msg string) {
	log.SetOutput(warnFile)
	log.WithFields(log.Fields{
		//"animal": "walrus",
		//"size":   10,
	}).Warn(msg)
}

func LogError(msg string) {
	log.SetOutput(errorFile)
	log.WithFields(log.Fields{
		//"animal": "walrus",
		//"size":   10,
	}).Error(msg)
}

/*
func Logger() *logrus.Logger {
	now := time.Now()
	logFilePath := ""
	if dir, err := os.Getwd(); err == nil {
		logFilePath = dir + "/logs/"
	}
	if err := os.MkdirAll(logFilePath, 0777); err != nil {
		fmt.Println(err.Error())
	}
	logFileName := now.Format("2006-01-02") + ".logs"
	//日志文件
	fileName := path.Join(logFilePath, logFileName)
	if _, err := os.Stat(fileName); err != nil {
		if _, err := os.Create(fileName); err != nil {
			fmt.Println(err.Error())
		}
	}
	//写入文件
	src, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		fmt.Println("err", err)
	}

	//实例化
	logger := logrus.New()

	//设置输出
	logger.Out = src

	//设置日志级别
	logger.SetLevel(logrus.DebugLevel)

	//设置日志格式
	logger.SetFormatter(&logrus.TextFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
	})
	return logger
}*/
