package util

import (
	"crypto/md5"
	"fmt"
	"io"
)

func mdFormat(data string) string {
	h := md5.New()
	_, _ = io.WriteString(h, data)
	return fmt.Sprintf("%x", h.Sum(nil))
}

func ValidatePasswd(plainPassWord, passWord string) bool {
	return mdFormat(plainPassWord) == passWord
}

func MakePassWord(plainPassWord string) string {
	return mdFormat(plainPassWord)
}
