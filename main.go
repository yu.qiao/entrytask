package main

import (
	"entryTest/controller"
	"entryTest/util"
	"html/template"
	"log"
	"net/http"
)

func registerView() {
	tpl, err := template.ParseGlob("./view/**/*")
	if err != nil {
		util.LogError(err.Error())
	}
	for _, v := range tpl.Templates() {
		tplName := v.Name()
		//fmt.Println(tplName)
		http.HandleFunc(tplName, func(writer http.ResponseWriter, request *http.Request) {
			tpl.ExecuteTemplate(writer, tplName, nil)
		})
	}
}

func main() {
	http.HandleFunc("/user/login", controller.UserLogin)
	http.HandleFunc("/user/register", controller.UserRegister)
	http.HandleFunc("/contact/addfriend", controller.AddFriend)
	http.HandleFunc("/contact/loadfriend", controller.LoadFriend)
	http.HandleFunc("/chat", controller.Chat)

	http.Handle("/asset/", http.FileServer(http.Dir(".")))
	http.Handle("/resource/", http.FileServer(http.Dir(".")))
	registerView()
	log.Fatal(http.ListenAndServe(":9090", nil))
}
