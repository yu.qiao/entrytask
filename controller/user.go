package controller

import (
	"entryTest/model"
	"entryTest/service"
	"entryTest/util"
	"fmt"
	"net/http"
)

var UserService service.UserService

func UserRegister(writer http.ResponseWriter, request *http.Request) {
	var user model.User
	errBind := util.Bind(request, &user)
	fmt.Println(errBind)
	fmt.Println(user)
	user, err := UserService.Register(user.UserName, user.PassWord, user.NickName)
	if err != nil {
		util.RespFail(writer, err.Error())
	} else {
		util.RespOk(writer, user, "")
	}
}

func UserLogin(writer http.ResponseWriter, request *http.Request) {
	_ = request.ParseForm()

	userName := request.PostForm.Get("user_name")
	plainPWD := request.PostForm.Get("pass_word")

	if len(userName) == 0 || len(plainPWD) == 0 {
		util.RespFail(writer, "username or password incorrect")
	}

	loginUser, err := UserService.Login(userName, plainPWD)
	if err != nil {
		util.RespFail(writer, err.Error())
	} else {
		util.RespOk(writer, loginUser, "")
	}
}
