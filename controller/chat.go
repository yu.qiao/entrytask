package controller

import (
	"encoding/json"
	"entryTest/model"
	"entryTest/service"
	"entryTest/util"
	"fmt"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

var messageService = service.MessageService{}
var contactService = service.ContactService{}

type Node struct {
	Conn      *websocket.Conn
	DataQueue chan []byte
}

func dispatch(data []byte) {
	msg := model.Message{}
	err := json.Unmarshal(data, &msg)
	if err != nil {
		util.LogError(err.Error())
		return
	}
	util.LogInfo("Chat/dispatch senderID:" +
		strconv.FormatUint(msg.SenderID, 10) +
		"  targetID: " +
		strconv.FormatUint(msg.TargetID, 10) +
		msg.Msg)

	switch msg.Msg {
	case "his":
		historyMsg(msg.SenderID, msg.TargetID)
	case "echo":
		echoStatus(msg.SenderID, msg.TargetID)
	case "set1":
		setStatus(msg.SenderID, msg.TargetID, 1)
	case "set2":
		setStatus(msg.SenderID, msg.TargetID, 2)
	case "set3":
		setStatus(msg.SenderID, msg.TargetID, 3)
	default:
		addMessage(msg.SenderID, msg.TargetID, msg.Msg)
		sendMsg(msg.TargetID, data)
	}

	/*if msg.Msg == "his" {
		historyMsg(msg.SenderID, msg.TargetID)
	}else {
		addMessage(msg.SenderID, msg.TargetID, msg.Msg)
		sendMsg(msg.TargetID, data)
	}*/

}

var clientMap = make(map[uint64][]*Node, 10)

var rw sync.RWMutex

func Chat(writer http.ResponseWriter, request *http.Request) {

	//fixme
	/*
		send json
		sendProc: {"dstid":7,"cmd":10,"userid":6,"media":1,"content":"12"}

	*/

	query := request.URL.Query()

	//fixme
	fmt.Println("query:", query.Encode())

	id := query.Get("id")
	token := query.Get("token")
	userId, _ := strconv.ParseUint(id, 10, 64)

	isLegal := checkToken(userId, token)

	//fixme
	//fmt.Println("isLegal:", isLegal)

	conn, err := (&websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return isLegal
		},
	}).Upgrade(writer, request, nil)

	if err != nil {
		util.LogError(err.Error())
		return
	}

	//todo
	conn.SetCloseHandler(func(code int, text string) error {
		//fmt.Println("connClose")
		util.LogInfo("connClose")
		removeCloseConn(userId, conn)
		return nil
	})

	node := &Node{
		Conn:      conn,
		DataQueue: make(chan []byte, 50),
	}

	rw.Lock()
	clientMap[userId] = append(clientMap[userId], node)
	//fmt.Println("ws,ID:",userId,len(clientMap[userId]))
	rw.Unlock()

	go sendProc(node)
	go receiveProc(node)

	//sendMsg(userId, []byte("welcome!"))
}

func sendProc(node *Node) {
	for {
		select {
		case data := <-node.DataQueue:
			//fixme
			util.LogInfo("sendProc: " + string(data))
			fmt.Println("sendProc: " + string(data))
			if err := node.Conn.WriteMessage(websocket.TextMessage, data); err != nil {
				util.LogError(err.Error())
				return
			}
		}
	}
}

func receiveProc(node *Node) {
	for {
		_, data, err := node.Conn.ReadMessage()
		if err != nil {
			util.LogError(err.Error())
			return
		}
		//todo
		//fmt.Printf("recv<=%s", data)

		dispatch(data)
	}
}

func sendMsg(targetID uint64, msg []byte) {
	rw.RLock()
	defer rw.RUnlock()

	//fixme
	//fmt.Println("sendMsg: ", string(msg))
	//util.LogInfo("sendMsg: " + string(msg))
	for _, node := range clientMap[targetID] {
		node.DataQueue <- msg
	}

	//node, ok := clientMap[userId]
	//if ok {
	//	node.DataQueue <- msg
	//}
}

func checkToken(userId uint64, token string) bool {
	user := UserService.Find(userId)
	return user.Token == token
}

func removeCloseConn(userID uint64, conn *websocket.Conn) {
	rw.Lock()
	defer rw.Unlock()
	//fixme
	//util.LogInfo(strconv.FormatInt(int64(len(clientMap[userID])), 10))

	if connLen := len(clientMap[userID]); connLen == 1 {
		clientMap[userID] = clientMap[userID][0:0]
	} else {
		for i, c := range clientMap[userID] {
			if c.Conn == conn {
				//fixme
				//util.LogInfo("conn equal")
				clientMap[userID][i] = clientMap[userID][connLen-1]
				clientMap[userID] = clientMap[userID][:connLen-1]
				break
			}
		}
	}
	//fixme
	//util.LogInfo(strconv.FormatInt(int64(len(clientMap[userID])), 10))
}

/*
func remove(slice []int, s int) []int {
	return append(slice[:s], slice[s+1:]...)
}
func remove(s []int, i int) []int {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}
*/

func addMessage(senderID, targetID uint64, msg string) {
	if err := messageService.Add(senderID, targetID, msg); err != nil {
		util.LogWarn("addMessage: " + err.Error())
	}
}

/*
	send json
	sendProc: {"dstid":7,"cmd":10,"userid":6,"media":1,"content":"12"}
*/
type serverSend struct {
	TargetID uint64 `json:"dstid"`
	Cmd      int    `json:"cmd"`
	UserID   uint64 `json:"userid"`
	Media    int    `json:"media"`
	Content  string `json:"content"`
}

func newServerSend(targetID, userID uint64, content string) serverSend {
	return serverSend{
		TargetID: targetID,
		Cmd:      10,
		UserID:   userID,
		Media:    1,
		Content:  content,
	}
}

func historyMsgProc(reqID uint64, messages []model.Message) []model.Message {
	for i, msg := range messages {

		ctime := time.Unix(int64(msg.CreateTime), 0)
		if reqID == msg.TargetID {
			messages[i].Msg = ctime.Format("2006-01-02 15:04:05") + ", receive: " + msg.Msg
		} else {
			messages[i].TargetID, messages[i].SenderID = msg.SenderID, msg.TargetID
			messages[i].Msg = ctime.Format("2006-01-02 15:04:05") + ", send: " + msg.Msg
		}
	}
	return messages
}

func historyMsg(senderID, targetID uint64) {
	messages := historyMsgProc(senderID, messageService.Find(senderID, targetID))
	for _, msg := range messages {
		fmt.Println("chat/historyMsg, targetID:" + strconv.FormatUint(msg.TargetID, 10) + "  msg:" + msg.Msg)
		util.LogInfo("chat/historyMsg, targetID:" + strconv.FormatUint(msg.TargetID, 10) + "  msg:" + msg.Msg)
		newMsg := newServerSend(msg.TargetID, msg.SenderID, msg.Msg)
		if res, err := json.Marshal(newMsg); err == nil {
			sendMsg(msg.TargetID, res)
		} else {
			util.LogWarn("chat/historyMsg: " + err.Error())
		}
	}
}

var statusMap = map[int]string{
	1: "read",
	2: "unread",
	3: "top",
}

func echoStatus(senderID, targetID uint64) {
	statusKey := contactService.EchoStatus(senderID, targetID)
	content := "session status: " + statusMap[statusKey]
	newMsg := newServerSend(senderID, targetID, content)
	if res, err := json.Marshal(newMsg); err == nil {
		sendMsg(senderID, res)
	} else {
		util.LogWarn("chat/echoStatus: " + err.Error())
	}
}

func setStatus(senderID, targetID uint64, status int) {
	if err := contactService.SetStatus(senderID, targetID, status); err != nil {
		util.LogWarn("chat/setStatus: " + err.Error())
	}
}
