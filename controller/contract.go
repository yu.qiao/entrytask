package controller

import (
	"entryTest/args"
	"entryTest/service"
	"entryTest/util"
	"fmt"
	"net/http"
)

var concatService service.ContactService

func AddFriend(writer http.ResponseWriter, request *http.Request) {
	var arg args.AddNewMember
	fmt.Println("add friend")

	if err := util.Bind(request, &arg); err != nil {
		util.LogWarn("controller/contract/AddFriend: " + err.Error())
	}
	friend := concatService.SearchFriendByName(arg.DstName)
	if friend.ID == 0 {
		util.RespFail(writer, "non-existent")
	} else {

		err := concatService.AddFriend(arg.Userid, friend.ID)
		if err != nil {
			util.RespFail(writer, err.Error())
		} else {
			util.RespOk(writer, nil, "add friend success")
		}
	}
}

func LoadFriend(writer http.ResponseWriter, request *http.Request) {
	var arg args.ContactArg
	fmt.Println("load friend")

	if err := util.Bind(request, &arg); err != nil {
		util.LogWarn("controller/contract/LoadFriend:" + err.Error())
	}
	users := concatService.SearchFriend(arg.Userid)

	//fixme
	fmt.Println(users)

	util.RespOkList(writer, users, len(users))
}
