module entryTest

go 1.16

require (
	github.com/gorilla/websocket v1.4.2
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.5.1 // indirect
	golang.org/x/sys v0.0.0-20200116001909-b77594299b42 // indirect
	gopkg.in/fatih/set.v0 v0.2.1
	gopkg.in/yaml.v2 v2.2.8 // indirect
	//golang.org/x/net v0.0.0-20190503192946-f4e77d36d62c // indirect
	//gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	//gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
	gorm.io/driver/mysql v1.1.1
	gorm.io/gorm v1.21.11
)
