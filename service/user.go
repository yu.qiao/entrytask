package service

import (
	"entryTest/model"
	"entryTest/util"
	"errors"
	"fmt"
	"math/rand"
)

type UserService struct{}

// Register User Register
func (s *UserService) Register(userName, passWord, nickName string) (model.User, error) {
	registerUser := model.User{}
	db := model.GetDB()
	//fixme
	fmt.Println("registerUserName: ", userName)
	db.Select("id").Where("user_name = ?", userName).Take(&registerUser)

	if registerUser.ID > 0 {
		return registerUser, errors.New("user existed")
	}

	registerUser.UserName = userName
	registerUser.PassWord = util.MakePassWord(passWord)

	registerUser.NickName = nickName
	registerUser.Status = 2

	registerUser.Token = randomStr(32)

	var err error
	err = db.Create(&registerUser).Error
	return registerUser, err
}

// Login user login
func (s *UserService) Login(userName, passWord string) (model.User, error) {
	loginUser := model.User{}
	db := model.GetDB()
	db.Select("id,pass_word,nick_name,token").Where("user_name = ?", userName).Take(&loginUser)

	//fmt.Println(loginUser)
	if loginUser.ID == 0 {
		return loginUser, errors.New("user does not exist")
	}
	if !util.ValidatePasswd(passWord, loginUser.PassWord) {
		return loginUser, errors.New("passWord incorrect")
	}

	//token := randomStr(32)
	//loginUser.Token = token
	//db.Model(&loginUser).Update("token", token)

	return loginUser, nil
}

// Find user find
func (s *UserService) Find(userID uint64) model.User {
	findUser := model.User{}
	db := model.GetDB()
	db.First(&findUser, userID)
	return findUser
}

func randomStr(n int) string {
	letterByte := []byte("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]byte, n)
	for i := range b {
		b[i] = letterByte[rand.Intn(len(letterByte))]
	}

	return string(b)
}
