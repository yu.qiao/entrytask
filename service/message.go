package service

import (
	"entryTest/model"
	"entryTest/util"
	"fmt"
)

type MessageService struct{}

func (s *MessageService) Add(senderID, targetID uint64, msg string) error {
	db := model.GetDB()
	addMsg := model.Message{
		SenderID: senderID,
		TargetID: targetID,
		Msg:      msg,
	}

	return db.Create(&addMsg).Error
}

func (s *MessageService) Find(senderID, targetID uint64) []model.Message {
	messages := make([]model.Message, 10)

	db := model.GetDB()

	db.Select("sender_id,target_id,message,ctime").Where(map[string]interface{}{"sender_id": senderID, "target_id": targetID}).
		Or(map[string]interface{}{"sender_id": targetID, "target_id": senderID}).
		Order("id asc").
		Limit(20).
		Find(&messages)
	fmt.Println("messagesFind")
	fmt.Println(messages)
	if db.Error != nil {
		util.LogInfo(db.Error.Error())
	}

	return messages
}
