package service

import (
	"entryTest/model"
	"errors"
)

type ContactService struct{}

func (c *ContactService) AddFriend(ID1 uint64, ID2 uint64) error {
	if ID1 == ID2 {
		return errors.New("user ID1 == ID2")
	}

	friend := model.Session{}
	db := model.GetDB()
	db.Select("ID").Where(map[string]interface{}{"user_id": ID1, "friend_id": ID2}).Take(&friend)

	if friend.ID > 0 {
		return errors.New("friend added")
	}

	//transaction
	tx := db.Begin()

	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	if err := tx.Error; err != nil {
		return err
	}

	if err := tx.Create(&model.Session{UserID: ID1, FriendID: ID2}).Error; err != nil {
		tx.Rollback()
		return err
	}

	if err := tx.Create(&model.Session{UserID: ID2, FriendID: ID1}).Error; err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error

}

func (c *ContactService) SearchFriend(userId uint64) []model.User {
	friends := make([]model.Session, 10)
	objIds := make([]uint64, 0)

	db := model.GetDB()
	//fmt.Println("searchID:", userId)
	db.Select("friend_id").Where("user_id = ?", userId).Find(&friends)

	//fmt.Println("friends:", friends)

	for _, v := range friends {
		objIds = append(objIds, v.FriendID)
	}

	//fmt.Println(len(objIds),"   ",objIds)

	users := make([]model.User, 0)
	if len(objIds) == 0 {
		return users
	}

	db.Where(objIds).Find(&users)
	//fmt.Println(users)
	return users
}

func (c *ContactService) SearchFriendByName(userName string) model.User {
	user := model.User{}

	model.GetDB().Where("user_name = ?", userName).Take(&user)
	return user
}

func (c *ContactService) EchoStatus(userID, friendID uint64) int {
	session := model.Session{}
	model.GetDB().Select("status").
		Where(map[string]interface{}{"user_id": userID, "friend_id": friendID}).Take(&session)
	return session.Status
}

func (c *ContactService) SetStatus(userID, friendID uint64, statusVal int) error {
	session := model.Session{}
	model.GetDB().Model(&session).
		Where(map[string]interface{}{"user_id": userID, "friend_id": friendID}).Update("status", statusVal)

	if c.EchoStatus(userID, friendID) == statusVal {
		return nil
	} else {
		return model.GetDB().Error
	}

}
